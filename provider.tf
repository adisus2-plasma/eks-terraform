provider "aws" {
  access_key = "${env.access_key}"
  secret_key = "${env.secret_key}"
  region  = "ap-southeast-1"
}